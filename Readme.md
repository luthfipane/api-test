# Step iport project
1. Install Katalon Studio [Download](https://docs.katalon.com/katalon-studio/docs/getting-started.html "katalon installer")
2. Clone my repository by this link [Repo](https://gitlab.com/luthfipane/api-test.git)
3. Open project that name "M.luthfi Rasyid Pane" ![alt text](ss1.png "Title")
4. Create file under folder Test Cases > New test case with name "login_test" ![alt text](ss2.png "Title")
5. the code on login_test is : 
```
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CucumberKW.runFeatureFile('Include/features/login_test.feature')
```
6. Create Test suite and choose the login test file ![alt text](ss3.png "Title")
7. Run the test with play button on the top ![alt text](ss3.png "Title")
8. We cant run on command line because katalon on command line is need to subscription for using Katalon Run Time Engine. we are currently using free katalon version