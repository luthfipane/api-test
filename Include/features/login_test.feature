Feature: Login user with email and password is combination of first name and last name

  Scenario: Get list of user, and passed the email, first name, and last name to the login payload
    Given I try to get the list of users
    Then I login using email id and password
 
  Scenario Outline: User passes incorrect email and password, and validate the error message and error code
  	Given I try to login with '<email>' and '<password>'
  	Then I get error message '<error>' with errorcode '<errorCode>'
  	Examples: 
  		| email 						| password 			| error 																					 	| errorCode |
			|	george.bluth@.in	|	George Bluth 	|	Note: Only defined users succeed registration		 	| 400			 	|
			|	george.bluth@.in	|								|	Missing password																	| 400				|
			|									  |								| Missing email or username													| 400				|
			|										| AnyPassword		|	Missing email or username													| 400				|