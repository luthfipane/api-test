import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import com.kms.katalon.core.testobject.RestRequestObjectBuilder
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static org.assertj.core.api.Assertions.*
import groovy.json.JsonSlurper

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class ApiTest {
	String response;
	static int statusCode;
	
	@Given("I try to get the list of users")
	def I_try_to_get_the_list_of_user() {
		response = ApiTest.apiClient("GET", "https://reqres.in/api/users", "");
		JsonSlurper jsonSlurper = new JsonSlurper();
		def result = jsonSlurper.parseText(response);
		assertThat(statusCode == 200);
	}

	@When("I login using email id and password")
	def I_login_using_email_id_and_password() {
		JsonSlurper jsonSlurper = new JsonSlurper();
		String id, body, regResponse;
		def result = jsonSlurper.parseText(response);
		def regResult;
		for (int i=0; i < result.data.size(); i++) {
			id = result.data[i].id;
			body = "{\"email\": \"" + result.data[i].email +"\", \"password\": \""+ result.data[i].first_name + " " + result.data[i].last_name + "\"}";
			regResponse = ApiTest.apiClient("POST", "https://reqres.in/api/register", body);
			regResult = jsonSlurper.parseText(regResponse);
			assertThat(id).isEqualTo(regResult.id.toString());
			assertThat(statusCode == 200);
		}
	}

	@Given("I try to login with '(.*)' and '(.*)'")
	def I_try_to_login_with_email_and_password(String email, String password) {
		String body;
		body = "{\"email\": \"" + email +"\", \"password\": \""+ password +"\"}";
		response = ApiTest.apiClient("POST", "https://reqres.in/api/register", body);
	}
	
	@Then("I get error message '(.*)' with errorcode '(.*)'")
	def I_the_error_with_error_code(String error, String errorCode) {
		JsonSlurper jsonSlurper = new JsonSlurper();
		def result = jsonSlurper.parseText(response);
		assertThat(result.error).isEqualTo(error);
		assertThat(statusCode.toString()).isEqualTo(errorCode);
	}
	
	public static String apiClient(String method, String endPoint, String body) {
		RestRequestObjectBuilder builder = new RestRequestObjectBuilder();
		builder.withRestUrl(endPoint);
		builder.withRestRequestMethod(method);
		List<String> headers = new ArrayList<String>();
		headers.add(new TestObjectProperty("accept-api-version", ConditionType.EQUALS, "resource=1.0, protocol=1.0"));
		if(!body.equals(null) || body.equals("")){
			builder.withTextBodyContent(body);
			headers.add(new TestObjectProperty("Content-Type", ConditionType.EQUALS, "application/json"));
			println("---TEST IF---")
		}
		builder.withHttpHeaders(headers);
		RequestObject request = builder.build();
		ResponseObject response = WS.sendRequest(request);
		WS.comment("---Response body: " + response.getResponseBodyContent());
		WS.comment("---Response header: [Status code=" + response.getStatusCode() + ", Content-Type=" + response.getContentType() + "]");
		statusCode = response.getStatusCode();
		return response.getResponseBodyContent().toString();
	}
}